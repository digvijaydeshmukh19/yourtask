import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { StudComponent } from './stud/stud.component';
import { FirstComponent } from './first/first.component';
//import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    AppComponent,
    StudComponent,
    FirstComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot([
      {path:'',redirectTo:'',pathMatch:'full'},
      //{path:'app-root',component:AppComponent},
      {path:'app-first',component:FirstComponent},
      {path:'app-stud',component:StudComponent}
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
