import { Component } from '@angular/core';

@Component({
  selector: 'app-stud',
  templateUrl: './stud.component.html',
  styleUrls: ['./stud.component.css']
})
export class StudComponent {


  student=[
      {fname:'Digvijay', lname:'deshmukh'},
      {fname:'ajinkya', lname:'rananavre'},
      {fname:'sumit', lname:'patil'}
  ];

    demo:any={};
    demo1:any={};
    
    addstud(){
      this.student.push(this.demo);
      this.demo={};
      
    }

    deletestud(i){
      console.log(i);
      this.student.splice(i,1);
     
      
    }

    indexval;
    editstud(val){
      this.demo1.fname=this.student[val].fname;
      this.demo1.lname=this.student[val].lname;
      this.indexval=val;
    }
    updatestud(){
      let val=this.indexval;
      for(let i=0;i<this.student.length;i++){
        if(i==val){
        this.student[i]=this.demo1;
        this.demo1={};
        
        }
      }
    }
    
  

}
